const IMG_PATH = 'https://image.tmdb.org/t/p/w1280';

const form = document.getElementById('form');
const search = document.getElementById('search');
const main = document.getElementById('main');
const title = document.getElementById('title');

getMovies('/search?');

async function getMovies(url) {
    const res = await fetch(url);
    const data = await res.json();
    showMovies(data.movies.body.results);
}

function showMovies(movies) {
    main.innerHTML = '';
    movies.forEach(movie => {
        const { title, poster_path, vote_average, overview } = movie;
        if (poster_path && poster_path.includes('.jpg')) {
            const movieEl = document.createElement('div');
            movieEl.classList.add('movie');
            movieEl.innerHTML = `
                <img src="${IMG_PATH + poster_path}" alt="${title}"/>
                <div class="movie-info">
                    <h3>${title}</h3>
                    <span class="${scoreColor(vote_average)}">${vote_average}</span>
                </div>
                <div class="overview">
                <h3>Overview</h3>
                ${overview}
                </div>
            `;
            main.appendChild(movieEl);
        }
    });
}

form.addEventListener('submit', async (e) => {
    e.preventDefault();
    const searchTerm = search.value;
    if (searchTerm && searchTerm !== '') {
        getMovies('/search/?searchTerm=' + searchTerm);
        search.value = '';
    } else {
        window.location.reload();
    }
});

function scoreColor(score) {
    if (score >= 8) {
        return 'green';
    } else if (score >= 6) {
        return 'orange';
    }
    else if (score < 6) {
        return 'red';
    }
}

title.addEventListener('click', () => {
    getMovies('/search?');
})