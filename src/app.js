const express = require('express');
const path = require('path');
const request = require('postman-request');
const DotEnv = require('dotenv');

DotEnv.config({ path: '.env' });

const API_URL = `https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=${process.env.API_KEY}&page=1`;
const SEARCH_API = `https://api.themoviedb.org/3/search/movie?api_key=${process.env.API_KEY}&query="`;

const app = express();
const port = process.env.PORT || 3000;

const PublicDirPath = path.join(__dirname, '../public');

app.use(express.static(PublicDirPath));

app.get('/', async (req, res) => {
    // res.sendFile(PublicDirPath + '/index.html');
    // const searchTerm = req.query.searchTerm;
    // getMovies(searchTerm, (error, movies) => {
    //     res.send({ movies });
    // });
});

app.get('/search', async (req, res) => {
    const searchTerm = req.query.searchTerm;
    getMovies(searchTerm, (error, movies) => {
        if (error) {
            return res.send({ error });
        }
        res.send({ movies });
    });
});

const getMovies = (searchTerm, callback) => {
    const url = (searchTerm === undefined || searchTerm === '') ? API_URL : SEARCH_API + searchTerm;
    request({ url, json: true }, (error, response, body) => {
        if (error) {
            callback('Error occured' + error);
        }
        if (body.results.length === 0) {
            callback('No movies found mathing your request');
        }
        callback(undefined, { body });
    });

}

app.get('*', (req, res) => {
    res.send('<h1>Invalid page</h1>');
});

app.listen(port, () => {
    console.log('Server is up and running on http://localhost:' + port)
});